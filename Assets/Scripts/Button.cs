using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    Text resultTemp;
    float result = 0.0f;
    float tempSave;
    float multiplier = 1.0f;
    string operation;

    void Start()
    {
        resultTemp = GameObject.Find("Result").GetComponent<Text>();
    }

    public void writeToTextField()
    {
        resultTemp.text = "" + result;
    }

    public void BackSpace()
    {
        int resultB = (int)result / 10;
        result = resultB;
        writeToTextField();
    }
    public void cleanResultC()
    {
        multiplier = 1;
        result = 0.0f;
        tempSave = 0.0f;
        writeToTextField();
    }

    public void cleanResultCe()
    {
        multiplier = 1;
        resultTemp.text = "0";
        result = 0.0f;
    }

    public void PlusMinus()
    {
        result = -result;
        resultTemp.text = "" + result;
    }

    public void Sqrt()
    {
        result = Mathf.Sqrt(result);
        resultTemp.text = result.ToString();
    }

    public void Pow()
    {
        result = Mathf.Pow(result,2);
        resultTemp.text = result.ToString();
    }

    public void OneX()
    {
        result = 1/result;
        resultTemp.text = result.ToString();
    }

    public void Percent()
    {
        result = (tempSave * result) /100;
        resultTemp.text = result.ToString();
    }
    public void setMultiplier()
    {
        multiplier = 0.1f;
        resultTemp.text = "" + result + ",";
    }

    public void AddDigit(int digit)
    {
        if (multiplier == 1.0f)
        {
            result *= 10;
            result += digit;
        }
        else
        {
            result += digit * multiplier;
            multiplier /= 10;
        }

        writeToTextField();
    }
    public void saveOperation(string o)
    {
        operation = o;
        tempSave = result;
        result = 0.0f;
        multiplier = 1;
        resultTemp.text = operation;

    }
    public void CalcResult()
    {
        switch (operation)
        {
            case "+":
                result = tempSave + result;
                break;
            case "-":
                result = tempSave - result;
                break;
            case "*":
                result = tempSave * result;
                break;
            case "%":
                result = tempSave % result;
                break;
            case "/":
                result = tempSave / result;
                break;
            case "1/x":
                result = 1 / tempSave;
                break;

        }
        multiplier = 1;
        operation = "";
        writeToTextField();
    }
}


